require("dotenv").config();
const express = require("express");
const app = express();
const { run } = require("./src/personalize");
app.listen(3000);

const aws = require("aws-sdk");
const multer = require("multer");
const multers3 = require("multer-s3");

aws.config.update({
  secretAccessKey: process.env.ACCESS_SECRET,
  accessKeyId: process.env.ACCESS_KEY,
  region: process.env.REGION,
});

const s3 = new aws.S3();
const BUCKET = process.env.BUCKET;
const upload = multer({
  storage: multers3({
    bucket: BUCKET,
    S3: S3,
    key: (req, file, cb) => {
      cb(null, file.originalname);
    },
  }),
});

app.post("/upload", upload.single("file"), (req, res) => {
  console.log(req.file);
  res.send("Successfully uploaded", req.file.location);
});

app.get("download/:filename", async (req, res) => {
  const filename = req.params.filename;
  let x = await s3.getObject({ Bucket: BUCKET, key: filename }).promise();
  res.send(x.Body);
});

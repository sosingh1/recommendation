import {
  personalizeClient,
  CreateBatchInferenceJobCommand,
} from "@aws-sdk/client-personalize";
// import { personalizeClient } from "./libs/personalizeClients.js";

export const createBatchInferenceJobParam = {
  jobName: process.env.BATCH_INFERENCE_JOB_NAME,
  jobInput: {
    s3DataSource: {
      path: process.env.BATCH_INFERENCE_JOB_INPUT_PATH,
    },
  },
  jobOutput: {
    s3DataDestination: {
      path: process.env.BATCH_INFERENCE_JOB_OUTPUT_PATH,
    },
  },
  roleArn: process.env.BATCH_INFERENCE_JOB_ROLE_ARN,
  solutionVersionArn: process.env.BATCH_INFERENCE_JOB_SOLUTION_VER_ARN,
  numResults: process.env.BATCH_INFERENCE_JOB_RESULT_NUM,
};

export const run = async () => {
  try {
    const response = await personalizeClient.send(
      new CreateBatchInferenceJobCommand(createBatchInferenceJobParam)
    );
    console.log("Success", response);
    return response;
  } catch (err) {
    console.log("Error", err);
  }
};
// run();
